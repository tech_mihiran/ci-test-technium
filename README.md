Technium Technical Test – Preparation

1.	Download & install XAMPP from here if you don’t have it installed (prefer PHP 7 or above).

•	Make sure Apache and MySql is running properly
•	Make sure you can access http://localhost/ properly
•	Make sure phpMyAdmin is working correctly http://localhost/phpmyadmin/

2.	Create a database called ci-test-technium-db in phpmyadmin
3.	Unzip ci-test-technium.zip into htdocs folder in XAMPP (C:/xampp/htdocs/)
4.	Access http://localhost/ci-test-technium/ and you should see the following and the database connection should be OK 

Troubleshooting

If you see any errors, make sure your phpMyAdmin user is root and password is empty (default username and password)
Also make sure you created the correct database ci-test-technium-db
If you still get an error, and unable to solve by yourself, please send an screen shot of the error to mihiran@technium.com.au 

Good Luck!
